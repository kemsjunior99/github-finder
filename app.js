// Init Github
const github = new Github;
const ui = new UI;

const searchInput = document.getElementById('searchInput');


searchInput.addEventListener('keyup', (e) => {
    const searchUserText = e.target.value;

    if (searchUserText !== '') {
        github.getUsers(searchUserText)
            .then(data => {
                if (data.profile.message === 'Not Found') {
                    // Show Alert
                    ui.showAlert('User Not Found!', 'alert alert-danger mt-3')


                } else {
                    ui.showProfile(data.profile);
                    ui.showRepos(data.repos);
                }
            })






    } else {
        ui.clearProfile();

    }


})